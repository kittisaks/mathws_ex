package th.or.gistda.mathws.correlation_service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.base.MathWsRequest;
import th.or.gistda.mathws.base.MathWsResponse;
import th.or.gistda.mathws.service.ServiceHandler_Test;

public class CorrelationServiceHandler_Test extends ServiceHandler_Test {
	
	private CorrelationServiceHandler corrServ = Mockito.spy(new CorrelationServiceHandler());
	
	@Test
	public void testHandler_OperationNormal_ExpectNoError() {
		//Prepare
		IMathWsRequest req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		Mockito.doNothing().when(corrServ).correlation_normal(Mockito.any(), Mockito.any(), Mockito.any());		
				
		req.setName("convolution");
		req.setOperation("normal");
		expectedRes.setService("convolution", "normal");
		expectedRes.setHttp(200, "Operation completed successfully");
		expectedRes.setInput(null, null);
		expectedRes.setOutput(null);
		
		//Execute
		corrServ.handle(req, res);
				
		//Verify
		assertEquals(expectedRes, res);
	}
	
	@Test
	public void testHandler_OperationInvalid_ExpectNoError() {
		//Prepare
		IMathWsRequest req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		Mockito.doNothing().when(corrServ).correlation_normal(Mockito.any(), Mockito.any(), Mockito.any());		
				
		req.setName("correlation");
		req.setOperation("InvalidOperation");
		expectedRes.setService("correlation", "InvalidOperation");
		expectedRes.setHttp(400, "Invalid correlation service operation");
		
		//Execute
		corrServ.handle(req, res);
				
		//Verify
		assertEquals(expectedRes, res);
	}

	@Test
	public void testCorrelation_normal_dataset_1() {
		
		//Prepare
		Double arrA[]    = new Double[] {1.0,   2.0,   3.0,    4.0,    5.0};
		Double arrB[]    = new Double[] {900.1, 890.2, 320.45, 101.26, 400.7};
		Double expArrC[] = new Double[] {6050.39};
		
		List<Double> inputA = Arrays.asList(arrA);
		List<Double> inputB = Arrays.asList(arrB);
		List<Double> expOutputC = Arrays.asList(expArrC);
		List<Double> outputC = new ArrayList();
		
		//Execute
		(new CorrelationServiceHandler()).correlation_normal(inputA, inputB, outputC);
		
		//Verify
		assertTrue(verifyErrorTolerance(expOutputC, outputC));
	}
	
	@Test
	public void testCorrelation_normal_dataset_2() {
		
		//Prepare
		Double arrA[]    = new Double[] {1.0,   2.0,    3.0,    -4.0,   5.0};
		Double arrB[]    = new Double[] {900.1, -890.2, 320.45, 101.26, 400.7};
		Double expArrC[] = new Double[] {1679.51};
		
		List<Double> inputA = Arrays.asList(arrA);
		List<Double> inputB = Arrays.asList(arrB);
		List<Double> expOutputC = Arrays.asList(expArrC);
		List<Double> outputC = new ArrayList();
		
		//Execute
		(new CorrelationServiceHandler()).correlation_normal(inputA, inputB, outputC);
		
		//Verify
		assertTrue(verifyErrorTolerance(expOutputC, outputC));
	}
	
	@Test
	public void testCorrelation_normal_dataset_3() {
		
		//Prepare
		Double arrA[]    = new Double[] {-1.0,   -2.0,    -3.0,    -4.0,   -5.0};
		Double arrB[]    = new Double[] {900.1, 890.2, 320.45, 101.26, 400.7};
		Double expArrC[] = new Double[] {-6050.39};
		
		List<Double> inputA = Arrays.asList(arrA);
		List<Double> inputB = Arrays.asList(arrB);
		List<Double> expOutputC = Arrays.asList(expArrC);
		List<Double> outputC = new ArrayList();
		
		//Execute
		(new CorrelationServiceHandler()).correlation_normal(inputA, inputB, outputC);
		
		//Verify
		assertTrue(verifyErrorTolerance(expOutputC, outputC));
	}
	
}
